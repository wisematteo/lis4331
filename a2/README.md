# LIS4331 - Advanced Mobile Web Application Development

## Matteo Wise

### Assignment 2 Requirements: 

1. Drop-down menu for total number of guests (including yourself): 1 –10
2. Drop-down menu for tip percentage (5% increments): 0 –25
3. Must add background color(s) or theme(10 pts)
4. Must create and displaylauncher icon image(10 pts)

README.md file should contain the following:                               

1. Course title, your name, assignment requirements, as per A1;           
2. Screenshotofrunning application’s unpopulateduser interface;
3. Screen shot of running application’s populateduser interface;                                           
                                                       



#### assignment screen shots:
| *Screenshot of running unpopulateduser - tip calculator*     | *Screenshot of runningpopulateduser- tip calculator *            |
|--------------------------------------------------------------|------------------------------------------------------------------|
|                                                              |                                                                  |  |                                                              |                                                                  |
| ![Android Studio - tip calculator](img/aaa_unpop.png)        | ![Android Studio - tip calculator](img/aaa_populated.png)        |






