# LIS4331
## Matteo Wise

### Project 2 design requirements: 
1. Insert at least five sample tasks (see p. 413, and screenshot below).
2. Test database class (see pp. 424, 425)
3. Must add background color(s) or theme
4. Create and display launcher icon image.

##README.mdfile should includethe following items (see images below):## 
1. Coursetitle, your name, assignment requirements, as per A1;
2. Screenshotofrunning application’sTask List;

##Deliverables##
1. Provide Bitbucketread-only access to lis4331repo, includelinks to the repos you created in the above tutorials in README.md, using Markdownsyntax(README.mdmust also include screenshots as per above.)
2. Blackboard Links:lis4331Bitbucketrepo

                            
|   * Simplifier App Visuals *                            | * Simplifier App. Visuals *                                  |
|---------------------------------------------------------|--------------------------------------------------------------|
| ![Simplifier](images/a.png)                             | ![Simplifier](images/b.png)                                  |
