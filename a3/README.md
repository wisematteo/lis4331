# LIS4331 - Advanced Mobile Web Application Development

## Matteo Wise

### Assignment 3 Requirements: 

1. Field to enter U.S. dollar amount: 1–100,000.
2. Must include toast notification if user enters out-of-range values.
3. Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must be vertically and horizontally aligned).
4. Must include correct sign for euros, pesos, and Canadian dollars.
5. Must add background color(s) or theme.
6. Create and displaylauncher icon image.
7. Create Splash/Loading Screen.

README.md file should contain the following:                               

1. Course title, your name, assignment requirements, as per A1;           
2. Screenshot of running application’s unpopulated user interface;
3. Screenshot of running application’s toast notifications;  
4. Screenshot of running application's converted currency user interface;                                        
                                                       



#### assignment screen shots:
| *Screenshot of running unpopulateduser - currency converter* | *Screenshot of running populated user - currency converter *     |
|--------------------------------------------------------------|------------------------------------------------------------------|
|                                                              |                                                                  |  |                                                              |                                                                  |
| ![Android Studio - tip calculator](image/a.png)              | ![Android Studio - tip calculator](image/b.png)                  |
| ![Android Studio - tip calculator](image/c.png)              | ![Android Studio - tip calculator](image/d.png)                  |






