# SSH README.md file

## Matteo Wise

### SSH not connected to server: Designed to find specific attributes of car models in a databse full of Hondas.


#maw_user@vma23:~/class/a07/q04$ more q04.sh
#!/bin/bash
#############################################################
#
#       Script Name:    q04.sh
#       Author:         matteo wise
#       Date:           12/8/18
#       Version:                1.0
#
#       Purpose:                Briefly describe in a few lines what 
#                   this script does: cool stuff
#
#############################################################

###if [ $# -ne 1 ]
###    then
###    echo "This requires a parameter"
###    echo " model name"
###    fi
###
###if [ ! -e model.sh ]; then
###    ln  -s ../q02/q02.sh model.sh
###    fi
###
###if [ ! -e env_mysql ]; then
###    ln  -s ../q01/env_mysql env_mysql
###    fi
###
###source env_mysql
###
##### variables 
###value=$1
###rp="report.tmp"
###if [ -e $rp ]; then
###    rm $rp
###    fi
###touch $rp
###
###cat > q04.sql << SQL
###use sales;
###select distinct(model_name) from sales_2010;
###SQL
###
###models=( $(mysql -u $MYSQLU -p$MYSQLP < q04.sql 2> /dev/null | grep -v model_name ) )
###
###found=0
######for m in "${models[@]}"
###do
###       if [ "$m" == "$value" ]
###        then
###                found=1
###        fi
###        done
###if [ $found -ne 1 ]; then
###        echo "invalid model name: $value"
###        if [ -e report.tmp ]
###        then
###                rm report.tmp
###        fi
###        exit
###    fi
###
###echo "Sales, Profit and Discount report for Model=$value" >> $rp
###echo "" >> $rp
###for type in all new used; do
###        echo "     $type Car Sales" >> $rp
###        echo "         Model    Number    Dealer     Sales      List     Profit    Discount    Average    Average  " >> $rp
###       echo "         Year      Sold      Cost      Value      Price     Total      Total      profit    Discount " >> $rp
###        echo "       ========  =======  =========  =========  =========  =======  ==========  ========  ========== " >> $rp
###
###    for year in {2010..2015}; do
###        salesdata=( $(./model.sh $year $value $type ) )
###                number=${salesdata[1]}
###                dcost=${salesdata[2]}
###                psold=${salesdata[3]}
###                plist=${salesdata[4]}
###        profit=$(($psold - $dcost))
###        profit=$(( $psold - $dcost )) 
###        discount=$(( $plist - $psold ))
###        ap=$( echo "scale=2;$profit/$number" | bc)
###        ad=$( echo "scale=2;$discount/$number" | bc)
###        printf "  %4s %9s %11s %10s %10s %10s %10s %8s %9s\n" $(echo "${salesdata[@]} $profit $discount $ap $ad") >> $rp
###    done
###    echo "       =====================================================================================================  " >> $rp
###    echo "" >> $rp
###done
###
###                              
                                                       



