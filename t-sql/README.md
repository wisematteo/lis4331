# LIS3784 - Intermediate Database Analysis
## Matteo Wise

### Assignment Menu GUI Requirements: 

1. T-SQL example database - property ownership. 

                            
|   *Pharmacy database example*                                                                                         |
|-----------------------------------------------------------------------------------------------------------------------|
| ![T-SQL tables](images/a.png)                                                                                         |


####SET ANSI_WARNINGS ON;
####GO
####
####use master;
####GO
####
####IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'maw14n')
####DROP DATABASE maw14n;
####GO
####
####IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'maw14n')
####CREATE DATABASE maw14n;
####GO
####
####use maw14n;
####GO
####
####--table dbo.patient
####
####IF OBJECT_ID (N'dbo.patient', N'U') IS NOT NULL
####DROP TABLE dbo.patient;
####GO
####
####CREATE TABLE dbo.patient
####(
####pat_id SMALLINT not null identity(1,1),
####pat_ssn int not null check (pat_ssn > 0 and pat_ssn <= 999999999),
####pat_fname VARCHAR(15) not null,
####pat_lname VARCHAR(30) not null,
####pat_street VARCHAR(30) not null,
####pat_city VARCHAR(30) not null,
####pat_state CHAR(2) not null DEFAULT 'FL',
####pat_zip INT not null check (pat_zip > 0 and pat_zip <= 999999999),
####pat_phone bigint not null check (pat_phone > 0 and pat_phone <= 9999999999),
########pat_email VARCHAR(100) NULL,
####pat_dob DATE NOT NULL,
####pat_gender CHAR(1) NOT NULL CHECK (pat_gender IN('m','f')),
####pat_notes VARCHAR(45) NULL,
####PRIMARY KEY(pat_id),
####
####CONSTRAINT ux_pat_ssn unique nonclustered (pat_ssn ASC)
####);
####
####--table dbo.medication
####
####IF OBJECT_ID (N'dbo.medication', N'U') IS NOT NULL
####DROP TABLE dbo.medication;
####
####CREATE TABLE dbo.medication
####(
####med_id SMALLINT NOT NULL identity(1,1),
####med_name VARCHAR(100) NOT NULL,
####med_price DECIMAL(5,2) NOT NULL CHECK (med_price > 0),
####med_shelf_life date not null,
####med_notes VARCHAR(255) null,
####PRIMARY KEY (med_id)
####);
####
####--table dbo.prescription
####
####IF OBJECT_ID (N'dbo.prescription', N'U') IS NOT NULL
####DROP TABLE dbo.prescription;
####
####CREATE TABLE dbo.prescription
####(
####pre_id SMALLINT not null identity(1,1),
####pat_id SMALLINT not null,
####med_id SMALLINT not null,
####pre_date date not null,
####pre_dosage VARCHAR(255) not null,
####pre_num_refills VARCHAR(3) NOT NULL,
####pre_notes VARCHAR(255) NULL,
####PRIMARY KEY (pre_id),
####
####CONSTRAINT ux_pat_id_med_id_pre_date unique nonclustered 
####(pat_id ASC, med_id ASC, pre_date ASC),
####
####CONSTRAINT fk_prescription_patient
####FOREIGN KEY (pat_id)
####REFERENCES dbo.patient (pat_id)
####ON DELETE NO ACTION
####ON UPDATE CASCADE,
####
####CONSTRAINT fk_prescription_medication
####FOREIGN KEY (med_id)
####REFERENCES dbo.medication (med_id)
####ON DELETE NO ACTION
####ON UPDATE CASCADE
####);
####
####-- table dbo.treatment
####
####IF OBJECT_ID (N'dbo.treatment', N'U') IS NOT NULL
####DROP TABLE dbo.treatment;
####
####CREATE TABLE dbo.treatment
####(
####trt_id SMALLINT not null identity(1,1),
####trt_name VARCHAR(255) NOT NULL,
####trt_price DECIMAL(8,2) NOT NULL CHECK (trt_price > 0),
####trt_notes VARCHAR(255) null,
####PRIMARY KEY (trt_id)
####);
####
####--table dbo.physician
####
####IF OBJECT_ID (N'dbo.physician', N'U') IS NOT NULL
####DROP TABLE dbo.physician;
####GO
####
####CREATE TABLE dbo.physician
####(
####phy_id SMALLINT NOT NULL identity(1,1),
####phy_specialty varchar(25) not null,
####phy_fname varchar(15) not null,
####phy_lname varchar(30) not null,
####phy_street VARCHAR(30) not null,
####phy_city VARCHAR(30) not null,
####phy_state CHAR(2) not null DEFAULT 'FL',
####phy_zip int not null check (phy_zip > 0 and phy_zip <= 999999999),
####phy_phone bigint not null check (phy_phone > 0 and phy_phone <= 9999999999),
####phy_fax bigint not null check (phy_fax > 0 and phy_fax <= 9999999999),
####phy_email varchar(100) null,
####phy_url varchar(100) null,
####phy_notes varchar(255) null,
####PRIMARY KEY (phy_id)
####);
####
####--table dbo.patient_treatment
####
####IF OBJECT_ID (N'dbo.patient_treatment', N'U') IS NOT NULL
####DROP TABLE dbo.patient_treatment;
####
####CREATE TABLE dbo.patient_treatment
####(
####ptr_id SMALLINT not null identity(1,1),
####pat_id SMALLINT NOT NULL,
####phy_id SMALLINT NOT NULL,
####trt_id SMALLINT not null,
####ptr_date DATE not null,
####ptr_start TIME(0) NOT NULL,
####ptr_end TIME(0) NOT NULL,
####ptr_results VARCHAR(255) null,
####ptr_notes VARCHAR(255) null,
####PRIMARY KEY (ptr_id),
####
####CONSTRAINT ux_pat_id_phy_id_trt_id_ptr_date unique nonclustered
####(pat_id ASC, phy_id ASC, trt_id ASC, ptr_date ASC),
####
####CONSTRAINT fk_patient_treatment_patient
####FOREIGN KEY (pat_id)
####REFERENCES dbo.patient (pat_id)
####ON DELETE NO ACTION
####ON UPDATE CASCADE,
####
####CONSTRAINT fk_patient_treatment_physician
####FOREIGN KEY (phy_id)
####REFERENCES dbo.physician (phy_id)
####ON DELETE NO ACTION
####ON UPDATE CASCADE,
####
####CONSTRAINT fk_patient_treatment_treatment
####FOREIGN KEY (trt_id)
####REFERENCES dbo.treatment (trt_id)
####ON DELETE NO ACTION 
####ON UPDATE CASCADE
####); 
####
####--table dbo.administration_lu
####--table dbo.administration_lu
####
####IF OBJECT_ID (N'dbo.administration_lu', N'U') IS NOT NULL
####DROP TABLE dbo.administration_lu;
####
####CREATE TABLE dbo.administration_lu
####(
####pre_id SMALLINT NOT NULL,
####ptr_id SMALLINT not null,
####PRIMARY KEY (pre_id, ptr_id),
####
####CONSTRAINT fk_administration_lu_prescription
####FOREIGN KEY (pre_id)
####REFERENCES dbo.prescription (pre_id)
####ON DELETE NO ACTION
####ON UPDATE CASCADE,
####
####CONSTRAINT fk_administration_lu_patient_treatment
####FOREIGN KEY (ptr_id)
####REFERENCES dbo.patient_treatment (ptr_id)
####ON DELETE NO ACTION
####ON UPDATE NO ACTION
####);
####
####--show tables
####SELECT * FROM information_schema.tables;
####
####EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all" 
####
####--data for tables
####
####--data for dbo.patient 
####
####insert into dbo.patient
####(pat_ssn, pat_fname, pat_lname, pat_street, pat_city, pat_state, pat_zip, pat_phone, pat_email, pat_dob, pat_gender, pat_notes)
####
####VALUES
####('123456789','Carla', 'Vanderbilt', '5133 3rd Road', 'Lake Worth', 'FL', '334671234', '5674892390', 'csweeny@yahoo.com','11-26-1961','f',null),
####('590123654','amanda','lindell','2241 W. pensacola street','Tallahassee','FL','323041234','9876543210','acm10c@my.fsu.edu','04-04-1981','f',null),
####('987456321','david','stephens','1293 Banana Code drive','pannama city','fl','323081234','8507623214','dstephens@comcast.net','05-15-1965','m',null),
####('365214986','chris','thrombough','987 learning drive','tallahassee','fl','323011234','5768941254','cthrombough@fu.edu','07-25-1969','m',null),
####('326598236','spencer','moore','787 tharpe road','tallahassee','fl','323061234','8764930213','spencer@my.fsu.edu','08-14-1990','m',null);
####
####--data for dbo.medication
####
####insert into dbo.medication
####(med_name, med_price, med_shelf_life, med_notes)
####
####VALUES
####('abilify',200.00,'06-23-2014',null),
####('aciphex',125.00,'06-24-2014',null),
####('actonel',250.00,'06-25-2014',null),
####('actoplus MET',412.00,'06-26-2014',null),
####('actos',89.00,'06-27-2014',null);
####
####--data for dbo.prescription
####
####insert into dbo.prescription
####(pat_id, med_id, pre_date, pre_dosage, pre_num_refills, pre_notes)
####
####VALUES
####(1,1,'01-01-2017','take one per day','1',null),
####(2,2,'01-01-2017','take one per day','1',null),
####(3,3,'01-01-2017','take one per day','1',null),
####(4,4,'01-01-2017','take one per day','1',null),
####(5,5,'01-01-2017','take one per day','rpn',null);
####
####--dbo.physician
####
####insert into dbo.physician
####(phy_specialty, phy_fname, phy_lname, phy_street, phy_city, phy_state,phy_zip,phy_phone,phy_fax, phy_notes)
####
####VALUES
####('family medicine','marie','martin','14 street','tallahassee','FL','32439','8501234557','0001234567', null),
####('chiropractor','mary','steeve','13 street','tallahassee','FL','32439','8501234547','0001234565', null),
####('cancer surgery','rick','scott','11 street','tallahassee','FL','32439','8501234537','0001234568', null),
####('dermatology','tim','john','12 street','tallahassee','FL','32439','8501234527','0001234568', null),
####('pediatrician','tom','doc','10 street','tallahassee','FL','32439','8501234517','0001234560', null);
####
####--data for dbo.treatment
####
####insert into dbo.treatment
####(trt_name,trt_price)
####
####VALUES
####('knee replacement',2000.99),
####('heart transplant',9999.99),
####('tonsils removed',5000.00),
####('stomach pumped',50.00),
####('skin graft',4956.00);
####
####--data for dbo.patient_treatment
####
####INSERT INTO dbo.patient_treatment
####(pat_id,phy_id,trt_id,ptr_date,ptr_start,ptr_end,ptr_results, ptr_notes)
####
####VALUES
####(1,5,1,'2011-12-23','07:08:09','10:12:15','failure', null),
####(2,4,2,'2011-12-23','07:08:09','10:12:15','success', null),
####(3,3,3,'2011-12-23','07:08:09','10:12:15','faiulure', null),
####(4,2,4,'2011-12-23','07:08:09','10:12:15','success', null),
####(5,1,5,'2011-12-23','07:08:09','10:12:15','failure', null);
####
####--data for dbo.administration_lu
####
####insert into dbo.administration_lu
####(pre_id,ptr_id)
####
####VALUES
####(1,5),
####(2,4),
####(3,3),
####(4,2),
####(5,1);
####
####EXEC sp_msforeachtable "alter table ? with check check constraint all"
####
####select * from dbo.patient;
####select * from dbo.medication;
####select * from dbo.prescription;
####select * from dbo.physician;
####select * from dbo.treatment;
####select * from dbo.patient_treatment;
####select * from dbo.administration_lu;
####
####
####--a
####
####use maw14n;
####go
####
####begin transaction;
####select pat_fname, pat_lname, pat_notes, med_name, med_price, med_shelf_life, pre_dosage, pre_num_refills
####from medication m
####join prescription pr on pr.med_id = m.med_id
####join patient p on pr.pat_id = p.pat_id
####order by med_price desc;
####commit;
####
####--b
####
####use maw14n;
####go
####
####IF OBJECT_ID (N'dbo.v_physician_patient_treatments',N'V') IS NOT NULL
####DROP VIEW dbo.v_physician_patient_treatments;
####go
####
####create view dbo.v_physician_patient_treatments as
####select phy_fname, phy_lname, trt_name, trt_price, ptr_results, ptr_date, ptr_start, ptr_end
####from physician p, patient_treatment pt, treatment t
####where p.phy_id=pt.phy_id
####and pt.trt_id=t.trt_id;
####go
####
####--c
####
####use maw14n;
####go
####
####select * from dbo.v_physician_patient_treatments;
####
####IF OBJECT_ID('AddRecord') IS NOT NULL
####DROP PROCEDURE AddRecord;
####go
####
####CREATE PROCEDURE AddRecord AS
####insert into dbo.patient_treatment
####(pat_id, phy_id, trt_id, ptr_date, ptr_start, ptr_end, ptr_results, ptr_notes)
####values (5, 5, 5,'2013-04-23','11:00:00','12:30:00','released','ok');
####
####select * from dbo.v_physician_patient_treatments;
####go
####
####EXEC AddRecord;
####
####--d
####
####use maw14n;
####
####begin transaction;
####select * from dbo.administration_lu;
####delete from dbo.administration_lu where pre_id = 5 and ptr_id = 10;
####select * from dbo.administration_lu;
####commit;
####
####--e
####
####use maw14n;
####go
####
####IF OBJECT_ID('dbo.UpdatePatient') IS NOT NULL
####DROP PROCEDURE dbo.UdatePatient;
####GO
####
####CREATE PROCEDURE dbo.UpdatePatient AS 
####
####select * from dbo.patient;
####
####update dbo.patient
####set pat_lname = 'Vanderbilt'
####where pat_id = 3;
####
####select * from dbo.patient;
####go
####
####EXEC dbo.UpdatePatient;
####go
####
####DROP PROCEDURE dbo.UpdatePatient;
####go
####
####--f
####
####EXEC sp_help 'dbo.patient_treatment';
####ALTER TABLE dbo.patient_treatment add ptr_prognosis varchar(255) NULL default 'testing';
####EXEC sp_help 'dbo.patient_treatment';
####
####--g
####
####IF OBJECT_ID('dbo.AddShowRecords') IS NOT NULL
####DROP PROCEDURE dbo.AddShowRecords;
####go
####
####CREATE PROCEDURE dbo.AddShowRecords AS
####
####select * from dbo.patient;
####
####insert into dbo.patient
####(pat_ssn, pat_fname, pat_lname, pat_street, pat_city, pat_state, pat_zip, pat_phone, pat_email, pat_dob, pat_gender, pat_notes)
####values ('756889432','John','Doe','123 Main St','Tallahassee','FL','32405','8507863241','jdoe@fsu.edu', '1959-05-10','M','testing notes');
####
####select * from dbo.patient;
####
####select phy_fname, phy_lname, pat_fname, pat_lname, trt_name, ptr_start, ptr_end, ptr_date
####from dbo.patient p
########join dbo.patient_treatment pt on p.pat_id=pt.pat_id
####join dbo.physician pn on pn.phy_id=pt.phy_id
####join dbo.treatment t on t.trt_id=pt.trt_id
####order by ptr_date desc;
####go
####
####EXEC dbo.AddShowRecords;
####go
####
####