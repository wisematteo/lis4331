# LIS4910 - IT Project
## Matteo Wise

### Assignment Menu GUI Requirements: 

1. Simplifier application project. 

                            
|   * Simplifier App Visuals *                            | * Simplifier App. Visuals *                                  |
|---------------------------------------------------------|--------------------------------------------------------------|
| ![Simplifier](images/a.png)                             | ![Simplifier](images/b.png)                                  |
| ![Simplifier](images/c.png)                             | ![Simplifier](images/d.png)                                  |
| ![Simplifier](images/e.png)                             | ![Simplifier](images/f.png)                                  |
| ![Simplifier](images/g.png)                             | ![Simplifier](images/h.png)                                  |