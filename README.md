# LIS 4331 - Advanced Mobile Web Application Development

## Matteo Wise

### LIS4331 Requirements

*Coursework Links:*

1. [A1 README.md](https://bitbucket.org/wisematteo/lis4331/src/master/a1/)
    * Install JDK.
    * Install Android Studio and create My App and Contacts App.
    * provide screenshots of installations
    * Create Bitbucket repo.
    * Complete bitcucket tutorials.(https://bitbucket.org/wisematteo/bitbucketstationlocations/src/master/)
    * provide git command descriptions. 
2. [A2 README.md](https://bitbucket.org/wisematteo/lis4331/src/master/a2/)
    * Create Android app.
    * Provide screenshot of completed app.
3. [A3 README.md](https://bitbucket.org/wisematteo/lis4331/src/master/a3/)
    * Created ERD based upon business rules.
    * Provide screenshot of completed ERD.
    * Provide DB resource links.
4. [A4 README.md](https://bitbucket.org/wisematteo/lis4331/src/7b78a2a663ac27031bc13a04c8cda5e5b9ae4ad2/a4/?at=master)
    * Create a morgage interest calculator.
    * Show unpopulated and populated screenshots.
5. [P1 README.md](https://bitbucket.org/wisematteo/lis4331/src/master/p1/) 
    * Music application unpopulated and populated screenshots
   [A5 README.md](https://bitbucket.org/wisematteo/lis4331/src/7b78a2a663ac27031bc13a04c8cda5e5b9ae4ad2/a5/?at=master)
    * Create an application Using an RSS feed.
    * Provide screenshots of running UI.
7. [P2 README.md](https://bitbucket.org/wisematteo/lis4331/src/f14e40f7f2aaa11733dc7418f6231ec6abab4304/p2/?at=master)     
    * Create a SQLite database
6. [MySQL README.md](https://bitbucket.org/wisematteo/lis4331/src/0f7a10897ea2eedd5443e245e78b8dad4b95c06f/mysql/BookStore/?at=master)
    * Shows the code for a MySQL database made for a class.
7. [T-SQL README.md](https://bitbucket.org/wisematteo/lis4331/src/7b78a2a663ac27031bc13a04c8cda5e5b9ae4ad2/t-sql/?at=master)
    * T-SQL database not connected to server.
    * Provide screenshot of running tables.
8. [SSH README.md](https://bitbucket.org/wisematteo/lis4331/src/7b78a2a663ac27031bc13a04c8cda5e5b9ae4ad2/ssh/?at=master)
    * SSH code not connnected to Server.
9. [Simplifier project README.md](https://bitbucket.org/wisematteo/lis4331/src/69f006764d2b71be6f84be9f9f1fa7e5e6035ca6/simplifier_development/simpproj/?at=master)
    * visual representation of Simplifier application code.
               