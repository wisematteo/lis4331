# LIS4331 - Advanced Mobile Web Application Development

## Matteo Wise

### Assignment 1 Requirements: 

1. Distributed Version Control With Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links 

| README.md file should contain the following:         | git commands w/short descriptions                                          |
|------------------------------------------------------|----------------------------------------------------------------------------|
|  Screenshot of running java. hello                   | 1. git init - creates a bare git repository.                               |
|  Screenshot of running android studio- my first app  | 2. git status - displays the state of the working directory.               |
|  Screenshot of running android studio- Contacts app  | 3. git add - create a new file in root directory, create a new file in     |
|  Git commands w/short descriptors                    | subdirectory, or update an existing file.                                  |
|                                                      | 4. git commit - records records to the repository.                         |
|                                                      | 5. git push - update remote repositories along with associated objects.    |
|                                                      | 6. git pull - used to fetch and download contenet from a remote repository.|
|                                                      | 7. git congif --get-all user.email (or user.name) : shows user name or     |
|                                                      | email.                                                                     |


#### assignment screen shots:

*Screenshot of running java Hello:*
![Java Hello](img/hellowrld.png)

*Screenshot of running Android Studio - My first App*

![Android Studio - my First App](img/andHelloWrld.png)

*Screenshot of Contacts App - Main Screen*

![Android Studio - my First App](img/contactone.png)

*Screenshots of Contact App - Main Screen*

![Android Studio - my First App](img/contacttwo.png)

*Bitbucket Tutorial - Station Locations:* 

[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/wisematteo/bitbucketstationlocations/src/master/)