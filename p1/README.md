# LIS4331 - Advanced Mobile Web Application Development

## Matteo Wise

### Project 1: 

1. Include splash screen image, app title, intro text.
2. Include artists’ images and media.
3. Images and buttons must be vertically and horizontally aligned.
4. Must add background color(s) or theme.
5. Create and displaylauncher icon image.


README.md file should contain the following:                               

1. Course title, your name, assignment requirements, as per A1;           
2. Screenshot of running application’s splash screen;
3. Screenshot of running application’s follow-up screen(with images and buttons);  
4. Screenshot of running application's play and pause user interfaces(with images and buttons);                                        
                                                       



#### assignment screen shots:
| *Screenshot of running unpopulateduser - Music Player*       | *Screenshot of running populated user - Music Player*            |
|--------------------------------------------------------------|------------------------------------------------------------------|
|                                                              |                                                                  |  |                                                              |                                                                  |
| ![Android Studio - Music Player](image/vsone.png)            | ![Android Studio - Music Player](image/vstwo.png)                |
| ![Android Studio - Music Player](image/vsthree.png)          | ![Android Studio - Music Player](image/vsfour.png)               |






