# LIS4331 - Advanced Mobile Web Application Development

## Matteo Wise

### MySQL: 

###Not connected to a database so cannot show tables 



###-- MySQL Workbench Forward Engineering
###
###SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
###SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
###SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
###
###-- -----------------------------------------------------
###-- Schema maw14n
###-- -----------------------------------------------------
###DROP SCHEMA IF EXISTS `maw14n` ;
###
###-- -----------------------------------------------------
###-- Schema maw14n
###-- -----------------------------------------------------
###CREATE SCHEMA IF NOT EXISTS `maw14n` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
###SHOW WARNINGS;
###USE `maw14n` ;
###
###-- -----------------------------------------------------
###-- Table `maw14n`.`member`
###-- -----------------------------------------------------
###DROP TABLE IF EXISTS `maw14n`.`member` ;
###
###SHOW WARNINGS;
###CREATE TABLE IF NOT EXISTS `maw14n`.`member` (
###  `mem_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
###  `mem_fname` VARCHAR(16) NOT NULL,
###  `mem_lname` VARCHAR(30) NOT NULL,
###  `mem_street` VARCHAR(30) NOT NULL,
###  `mem_city` VARCHAR(30) NOT NULL,
###  `mem_state` CHAR(2) NOT NULL,
###  `mem_zip` INT UNSIGNED NOT NULL,
###  `mem_phone` BIGINT NOT NULL,
###  `mem_email` VARCHAR(100) NULL,
###  `mem_notes` VARCHAR(255) NULL,
###  PRIMARY KEY (`mem_id`))
###ENGINE = InnoDB;
###
###SHOW WARNINGS;
###
###-- -----------------------------------------------------
###-- Table `maw14n`.`publisher`
###-- -----------------------------------------------------
###DROP TABLE IF EXISTS `maw14n`.`publisher` ;
###
###SHOW WARNINGS;
###CREATE TABLE IF NOT EXISTS `maw14n`.`publisher` (
###  `pub_id` SMALLINT UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
###  `pub_name` VARCHAR(45) NOT NULL,
###  `pub_street` VARCHAR(30) NOT NULL,
###  `pub_city` VARCHAR(30) NOT NULL,
###  `pub_state` CHAR(2) NOT NULL,
###  `pub_zip` INT UNSIGNED ZEROFILL NOT NULL,
###  `pub_phone` BIGINT UNSIGNED NOT NULL,
###  `pub_email` VARCHAR(100) NOT NULL,
###  `pub_url` VARCHAR(100) NOT NULL,
###  `pub_notes` VARCHAR(255) NULL,
###  PRIMARY KEY (`pub_id`))
###ENGINE = InnoDB;
###
###SHOW WARNINGS;
###
###-- -----------------------------------------------------
###-- Table `maw14n`.`book`
###-- -----------------------------------------------------
###DROP TABLE IF EXISTS `maw14n`.`book` ;
###
###SHOW WARNINGS;
###CREATE TABLE IF NOT EXISTS `maw14n`.`book` (
###  `bok_isbn` VARCHAR(13) NOT NULL,
###  `pub_id` SMALLINT UNSIGNED ZEROFILL NOT NULL,
###  `bok_title` VARCHAR(100) NOT NULL,
###  `bok_pub_date` DATE NOT NULL,
###  `bok_num_pages` SMALLINT UNSIGNED NOT NULL,
###  `bok_cost` DECIMAL(5,2) UNSIGNED NOT NULL,
###  `bok_price` DECIMAL(5,2) UNSIGNED NULL,
###  `bok_notes` VARCHAR(255) NULL,
###  PRIMARY KEY (`bok_isbn`),
###  INDEX `fk_book_publisher` (`pub_id` ASC),
###  CONSTRAINT `fk_pub_id`
###    FOREIGN KEY (`pub_id`)
###    REFERENCES `maw14n`.`publisher` (`pub_id`)
###    ON DELETE NO ACTION
###   ON UPDATE NO ACTION)
###ENGINE = InnoDB;
###
###SHOW WARNINGS;
###
###-- -----------------------------------------------------
###-- Table `maw14n`.`category`
###-- -----------------------------------------------------
###DROP TABLE IF EXISTS `maw14n`.`category` ;
###
###SHOW WARNINGS;
###CREATE TABLE IF NOT EXISTS `maw14n`.`category` (
###  `cat_id` SMALLINT NOT NULL AUTO_INCREMENT,
###  `cat_type` VARCHAR(45) NOT NULL,
###  `cat_notes` VARCHAR(255) NULL,
###  PRIMARY KEY (`cat_id`))
###ENGINE = InnoDB;
###
###SHOW WARNINGS;
###
###-- -----------------------------------------------------
###-- Table `maw14n`.`book category`
###-- -----------------------------------------------------
###DROP TABLE IF EXISTS `maw14n`.`book category` ;
###
###SHOW WARNINGS;
###CREATE TABLE IF NOT EXISTS `maw14n`.`book category` (
###  `bct_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
###  `bok_isbn` VARCHAR(13) NOT NULL,
###  `cat_id` SMALLINT NULL,
###  `bct_notes` VARCHAR(255) NULL,
###  PRIMARY KEY (`bct_id`),
###  INDEX `fk_book category_book1_idx` (`bok_isbn` ASC),
###  INDEX `fk_book category_category1_idx` (`cat_id` ASC),
###  CONSTRAINT `fk_book category_book1`
###   FOREIGN KEY (`bok_isbn`)
###    REFERENCES `maw14n`.`book` (`bok_isbn`)
###    ON DELETE NO ACTION
###    ON UPDATE NO ACTION,
###  CONSTRAINT `fk_book category_category1`
###    FOREIGN KEY (`cat_id`)
###    REFERENCES `maw14n`.`category` (`cat_id`)
###    ON DELETE NO ACTION
###    ON UPDATE NO ACTION)
###ENGINE = InnoDB;
###
###SHOW WARNINGS;
###
###-- -----------------------------------------------------
###-- Table `maw14n`.`author`
###-- -----------------------------------------------------
###DROP TABLE IF EXISTS `maw14n`.`author` ;
###
###SHOW WARNINGS;
###CREATE TABLE IF NOT EXISTS `maw14n`.`author` (
###  `aut_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
###  `aut_fname` VARCHAR(16) NOT NULL,
###  `aut_lname` VARCHAR(30) NOT NULL,
###  `aut_street` VARCHAR(30) NOT NULL,
###  `aut_city` VARCHAR(30) NOT NULL,
###  `aut_state` CHAR(2) NOT NULL,
###  `aut_zip` INT NOT NULL,
###  `aut_phone` BIGINT NOT NULL,
###  `aut_email` VARCHAR(100) NOT NULL,
###  `aut_url` VARCHAR(100) NOT NULL,
###  `aut_notes` VARCHAR(255) NULL,
###  PRIMARY KEY (`aut_id`))
###ENGINE = InnoDB;
###
###SHOW WARNINGS;
###
###-- -----------------------------------------------------
###-- Table `maw14n`.`attribution`
###-- -----------------------------------------------------
###DROP TABLE IF EXISTS `maw14n`.`attribution` ;
###
###SHOW WARNINGS;
###CREATE TABLE IF NOT EXISTS `maw14n`.`attribution` (
###  `att_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
###  `aut_id` MEDIUMINT UNSIGNED NOT NULL,
###  `bok_isbn` VARCHAR(13) NOT NULL,
###  `att_notes` VARCHAR(255) NULL,
###  PRIMARY KEY (`att_id`),
###  INDEX `fk_attribution_book1_idx` (`bok_isbn` ASC),
### INDEX `fk_attribution_author1_idx` (`aut_id` ASC),
###  UNIQUE INDEX `ux_bok_isbn_cat` (`att_id` ASC, `aut_id` ASC, `bok_isbn` ASC),
###  CONSTRAINT `fk_attribution_book1`
###    FOREIGN KEY (`bok_isbn`)
###    REFERENCES `maw14n`.`book` (`bok_isbn`)
###    ON DELETE NO ACTION
###    ON UPDATE NO ACTION,
###  CONSTRAINT `fk_attribution_author1`
###    FOREIGN KEY (`aut_id`)
###    REFERENCES `maw14n`.`author` (`aut_id`)
###   ON DELETE NO ACTION
###    ON UPDATE NO ACTION)
###ENGINE = InnoDB;
###
###SHOW WARNINGS;
###
###-- -----------------------------------------------------
###-- Table `maw14n`.`loaner`
###-- -----------------------------------------------------
###DROP TABLE IF EXISTS `maw14n`.`loaner` ;
###
###SHOW WARNINGS;
###CREATE TABLE IF NOT EXISTS `maw14n`.`loaner` (
###  `loan_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
###  `mem_id` SMALLINT UNSIGNED NOT NULL,
###  `book_isbn` VARCHAR(13) NOT NULL,
###  INDEX `fk_loaner_book2_idx` (`book_isbn` ASC),
###  PRIMARY KEY (`loan_id`),
###  INDEX `fk_loaner_member1_idx` (`mem_id` ASC),
###  CONSTRAINT `fk_loaner_book2`
###    FOREIGN KEY (`book_isbn`)
###    REFERENCES `maw14n`.`book` (`bok_isbn`)
###    ON DELETE NO ACTION
###    ON UPDATE NO ACTION,
###  CONSTRAINT `fk_loaner_member1`
###    FOREIGN KEY (`mem_id`)
###    REFERENCES `maw14n`.`member` (`mem_id`)
###    ON DELETE NO ACTION
###    ON UPDATE NO ACTION)
###ENGINE = InnoDB;
###
###SHOW WARNINGS;
###
###SET SQL_MODE=@OLD_SQL_MODE;
###SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
###SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
###
###-- -----------------------------------------------------
###-- Data for table `maw14n`.`member`
###-- -----------------------------------------------------
###START TRANSACTION;
###USE `maw14n`;
###INSERT INTO `maw14n`.`member` (`mem_id`, `mem_fname`, `mem_lname`, `mem_street`, `mem_city`, `mem_state`, `mem_zip`, `mem_phone`, `mem_email`, `mem_notes`) VALUES (DEFAULT, 'Jake', 'Smith ', 'Bush', 'Boston', 'NJ', 9876511, 8501112222, NULL, NULL);
###INSERT INTO `maw14n`.`member` (`mem_id`, `mem_fname`, `mem_lname`, `mem_street`, `mem_city`, `mem_state`, `mem_zip`, `mem_phone`, `mem_email`, `mem_notes`) VALUES (DEFAULT, 'Jeff', 'Salley', 'Martin', 'Buffalo ', 'NY', 9988771, 8502221111, NULL, NULL);
###INSERT INTO `maw14n`.`member` (`mem_id`, `mem_fname`, `mem_lname`, `mem_street`, `mem_city`, `mem_state`, `mem_zip`, `mem_phone`, `mem_email`, `mem_notes`) VALUES (DEFAULT, 'Allen', 'Sue', 'Yeller', 'Sopchoppy', 'FL', 1122339, 8504443333, NULL, NULL);
###INSERT INTO `maw14n`.`member` (`mem_id`, `mem_fname`, `mem_lname`, `mem_street`, `mem_city`, `mem_state`, `mem_zip`, `mem_phone`, `mem_email`, `mem_notes`) VALUES (DEFAULT, 'Drew', 'Sanders', 'Library', 'Tallahassee', 'FL', 1234099, 8505556666, NULL, NULL);
###INSERT INTO `maw14n`.`member` (`mem_id`, `mem_fname`, `mem_lname`, `mem_street`, `mem_city`, `mem_state`, `mem_zip`, `mem_phone`, `mem_email`, `mem_notes`) VALUES (DEFAULT, 'Jake', 'Salamander', 'Kansas', 'Huston', 'TX', 4561234, 8509876543, NULL, NULL);
###
###COMMIT;
###
###
###-- -----------------------------------------------------
###-- Data for table `maw14n`.`publisher`
###-- -----------------------------------------------------
###START TRANSACTION;
###USE `maw14n`;
###INSERT INTO `maw14n`.`publisher` (`pub_id`, `pub_name`, `pub_street`, `pub_city`, `pub_state`, `pub_zip`, `pub_phone`, `pub_email`, `pub_url`, `pub_notes`) VALUES (DEFAULT, 'bac', 'leanord', 'sunrise', 'FL', 77771, 3332221111, 'bac@email.com', 'bac.com', NULL);
###INSERT INTO `maw14n`.`publisher` (`pub_id`, `pub_name`, `pub_street`, `pub_city`, `pub_state`, `pub_zip`, `pub_phone`, `pub_email`, `pub_url`, `pub_notes`) VALUES (DEFAULT, 'vas', 'grape', 'weston', 'FL', 77772, 5555555555, 'vas@email.com', 'vas.com', NULL);
###INSERT INTO `maw14n`.`publisher` (`pub_id`, `pub_name`, `pub_street`, `pub_city`, `pub_state`, `pub_zip`, `pub_phone`, `pub_email`, `pub_url`, `pub_notes`) VALUES (DEFAULT, 'dss', 'tree', 'cincinnati', 'CT', 77773, 4444445555, 'dss@email.com', 'dss.com', NULL);
###INSERT INTO `maw14n`.`publisher` (`pub_id`, `pub_name`, `pub_street`, `pub_city`, `pub_state`, `pub_zip`, `pub_phone`, `pub_email`, `pub_url`, `pub_notes`) VALUES (DEFAULT, 'ttt', 'see', 'buffalo', 'NY', 77774, 8768760987, 'ttt@emali.com', 'ttt.com', NULL);
###INSERT INTO `maw14n`.`publisher` (`pub_id`, `pub_name`, `pub_street`, `pub_city`, `pub_state`, `pub_zip`, `pub_phone`, `pub_email`, `pub_url`, `pub_notes`) VALUES (DEFAULT, 'rrr', 'flee', 'tallahassee', 'FL', 77775, 4325436543, 'rrr@email.com', 'rrr.com', NULL);
###
###COMMIT;
###
###
###-- -----------------------------------------------------
###-- Data for table `maw14n`.`book`
###-- -----------------------------------------------------
###START TRANSACTION;
###USE `maw14n`;
###INSERT INTO `maw14n`.`book` (`bok_isbn`, `pub_id`, `bok_title`, `bok_pub_date`, `bok_num_pages`, `bok_cost`, `bok_price`, `bok_notes`) VALUES ('1234567890', , 'Roses are Red and blue', '1992-02-19', 500, 10.00, 20.00, NULL);
###INSERT INTO `maw14n`.`book` (`bok_isbn`, `pub_id`, `bok_title`, `bok_pub_date`, `bok_num_pages`, `bok_cost`, `bok_price`, `bok_notes`) VALUES ('1234567810', , 'Blue Roses are Red', '2000-01-19', 499, 9.00, 18.00, NULL);
###INSERT INTO `maw14n`.`book` (`bok_isbn`, `pub_id`, `bok_title`, `bok_pub_date`, `bok_num_pages`, `bok_cost`, `bok_price`, `bok_notes`) VALUES ('1234567811', , 'Red Roses are Blue', '1999-10-10', 501, 7.00, 14.00, NULL);
###INSERT INTO `maw14n`.`book` (`bok_isbn`, `pub_id`, `bok_title`, `bok_pub_date`, `bok_num_pages`, `bok_cost`, `bok_price`, `bok_notes`) VALUES ('1234567812', , 'Green Roses are Orange ', '2001-12-16', 498, 8.00, 16.00, NULL);
###INSERT INTO `maw14n`.`book` (`bok_isbn`, `pub_id`, `bok_title`, `bok_pub_date`, `bok_num_pages`, `bok_cost`, `bok_price`, `bok_notes`) VALUES ('1234567813', , 'Orange Roses are strange', '1996-15-26', 502, 7.00, 14.00, NULL);
###
###COMMIT;
###
###
###-- -----------------------------------------------------
###-- Data for table `maw14n`.`category`
###-- -----------------------------------------------------
###START TRANSACTION;
###USE `maw14n`;
###INSERT INTO `maw14n`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (DEFAULT, 'Horror', NULL);
###INSERT INTO `maw14n`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (DEFAULT, 'Romance', NULL);
###INSERT INTO `maw14n`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (DEFAULT, 'Thriller', NULL);
###INSERT INTO `maw14n`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (DEFAULT, 'Action', NULL);
###INSERT INTO `maw14n`.`category` (`cat_id`, `cat_type`, `cat_notes`) VALUES (DEFAULT, 'Horror', NULL);
###
###COMMIT;
###
###
###-- -----------------------------------------------------
###-- Data for table `maw14n`.`book category`
###-- -----------------------------------------------------
###START TRANSACTION;
###USE `maw14n`;
###INSERT INTO `maw14n`.`book category` (`bct_id`, `bok_isbn`, `cat_id`, `bct_notes`) VALUES (DEFAULT, '1234567890', NULL, NULL);
###INSERT INTO `maw14n`.`book category` (`bct_id`, `bok_isbn`, `cat_id`, `bct_notes`) VALUES (DEFAULT, '1234567810', NULL, NULL);
###INSERT INTO `maw14n`.`book category` (`bct_id`, `bok_isbn`, `cat_id`, `bct_notes`) VALUES (DEFAULT, '1234567811', NULL, NULL);
###INSERT INTO `maw14n`.`book category` (`bct_id`, `bok_isbn`, `cat_id`, `bct_notes`) VALUES (DEFAULT, '1234567812', NULL, NULL);
###INSERT INTO `maw14n`.`book category` (`bct_id`, `bok_isbn`, `cat_id`, `bct_notes`) VALUES (DEFAULT, '1234567813', NULL, NULL);
###
###COMMIT;
###
###
###-- -----------------------------------------------------
###-- Data for table `maw14n`.`author`
###-- -----------------------------------------------------
###START TRANSACTION;
###USE `maw14n`;
###INSERT INTO `maw14n`.`author` (`aut_id`, `aut_fname`, `aut_lname`, `aut_street`, `aut_city`, `aut_state`, `aut_zip`, `aut_phone`, `aut_email`, `aut_url`, `aut_notes`) VALUES (DEFAULT, 'Dale', 'Davis', 'Tree street', 'Buffalo', 'NY', 12345, 98798709870, 'dale@email.com', 'dale.com', NULL);
###INSERT INTO `maw14n`.`author` (`aut_id`, `aut_fname`, `aut_lname`, `aut_street`, `aut_city`, `aut_state`, `aut_zip`, `aut_phone`, `aut_email`, `aut_url`, `aut_notes`) VALUES (DEFAULT, 'Davis', 'Daleson', 'Grate lane', 'Vegas', 'NV', 12346, 1234448888, 'daleson@email.com', 'daleson.com', NULL);
###INSERT INTO `maw14n`.`author` (`aut_id`, `aut_fname`, `aut_lname`, `aut_street`, `aut_city`, `aut_state`, `aut_zip`, `aut_phone`, `aut_email`, `aut_url`, `aut_notes`) VALUES (DEFAULT, 'Dandruff', 'Hair', 'Harrier street', 'Sunrise', 'FL', 12347, 8887758886, 'hair@email.com', 'hair.com', NULL);
###INSERT INTO `maw14n`.`author` (`aut_id`, `aut_fname`, `aut_lname`, `aut_street`, `aut_city`, `aut_state`, `aut_zip`, `aut_phone`, `aut_email`, `aut_url`, `aut_notes`) VALUES (DEFAULT, 'Dave', 'Nodavis', 'Georgia', 'Mexico', 'NM', 12348, 1235556655, 'notdavis@email.com', 'notdavis.com', NULL);
###INSERT INTO `maw14n`.`author` (`aut_id`, `aut_fname`, `aut_lname`, `aut_street`, `aut_city`, `aut_state`, `aut_zip`, `aut_phone`, `aut_email`, `aut_url`, `aut_notes`) VALUES (DEFAULT, 'Draph', 'Football', 'College way', 'Preston', 'NY', 12349, 1119994444, 'football@email.com', 'football.com', NULL);
###
###COMMIT;
###
###
###-- -----------------------------------------------------
###-- Data for table `maw14n`.`attribution`
###-- -----------------------------------------------------
###START TRANSACTION;
###USE `maw14n`;
###INSERT INTO `maw14n`.`attribution` (`att_id`, `aut_id`, `bok_isbn`, `att_notes`) VALUES (DEFAULT, DEFAULT, '1234567890', NULL);
###INSERT INTO `maw14n`.`attribution` (`att_id`, `aut_id`, `bok_isbn`, `att_notes`) VALUES (DEFAULT, DEFAULT, '1234567810', NULL);
###INSERT INTO `maw14n`.`attribution` (`att_id`, `aut_id`, `bok_isbn`, `att_notes`) VALUES (DEFAULT, DEFAULT, '1234567811', NULL);
###INSERT INTO `maw14n`.`attribution` (`att_id`, `aut_id`, `bok_isbn`, `att_notes`) VALUES (DEFAULT, DEFAULT, '1234567812', NULL);
###INSERT INTO `maw14n`.`attribution` (`att_id`, `aut_id`, `bok_isbn`, `att_notes`) VALUES (DEFAULT, DEFAULT, '1234567813', NULL);
###
###COMMIT;
###
###
###-- -----------------------------------------------------
###-- Data for table `maw14n`.`loaner`
###-- -----------------------------------------------------
###START TRANSACTION;
###USE `maw14n`;
###INSERT INTO `maw14n`.`loaner` (`loan_id`, `mem_id`, `book_isbn`) VALUES (DEFAULT, , '1234567890');
###INSERT INTO `maw14n`.`loaner` (`loan_id`, `mem_id`, `book_isbn`) VALUES (DEFAULT, , '1234567810');
###INSERT INTO `maw14n`.`loaner` (`loan_id`, `mem_id`, `book_isbn`) VALUES (DEFAULT, , '1234567811');
###INSERT INTO `maw14n`.`loaner` (`loan_id`, `mem_id`, `book_isbn`) VALUES (DEFAULT, , '1234567812');
###INSERT INTO `maw14n`.`loaner` (`loan_id`, `mem_id`, `book_isbn`) VALUES (DEFAULT, , '1234567813');
###
###COMMIT;


           