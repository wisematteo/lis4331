# LIS4331 - Advanced Mobile Web Application Development

## Matteo Wise

### Assignment 4: 

1. Include splash screen image, app title, intro text.
2. Include appropriate images.
3. Must use persistent data: SharedPreferences.
4. Widgets and images must be vertically and horizontally aligned.
5. Must add background color(s) or theme.
6. Create and displaylauncher icon image.

README.md file should contain the following:                               

1. Coursetitle, your name, assignment requirements, as per A1;
2. Screenshotofrunning application’s splash screen;
3. Screenshotofrunning application’s invalidscreen(with appropriate image);
4. Screenshotsofrunning application’svalid screen(with appropriate image);                                       
                                                       



#### assignment screen shots:
| *Screenshot of running unpopulateduser - Mortgage Calculator* | *Screenshot of running populated user - Mortgage Calculator*     |
|-------------------------------------------------------------- |------------------------------------------------------------------|
|                                                               |                                                                  | |                                                               |                                                                  |
| ![Android Studio - Music Player](images/sc1.png)              | ![Android Studio - Music Player](images/sc2.png)                 |
| ![Android Studio - Music Player](images/sc3.png)              | ![Android Studio - Music Player](images/sc4.png)                 |






