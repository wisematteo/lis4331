# LIS4331 - Advanced Mobile Web Application Development

## Matteo Wise

### Assignment Menu GUI Requirements: 

1. GUI menu selection code.
                              
                                                       



#### assignment screen shots:
| *Screenshot of running unpopulateduser - GUI *               | *Screenshot of running populated user - GUI *                    |
|--------------------------------------------------------------|------------------------------------------------------------------|
|                                                              |                                                                  |  |                                                              |                                                                  |
| ![GUI - Menu](images/2019-04-10.png)                         | ![GUI - Menu](images/tree.png)                                   |
|                                                              | ![GUI - Menu](images/treetwo.png)                                |






