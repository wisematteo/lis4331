public class SimpleCalculator {

    public static void main(String[] args) {
        System.out.println("Program creates an array with the following numbers, ");
        System.out.println("and performs the operations below using user-defined methods.");
        System.out.println("");
        System.out.print("Numbers in the array are ");
        int[] myList = new int[]{12, 15, 34, 67, 4, 9, 10, 7, 13, 50};
 
       // Print all the array elements
       for (int i = 0; i < myList.length; i++) {
            
            System.out.print(myList[i] + " ");
       }

    System.out.println("");
       System.out.print("Numbers in reverse order are ");
    //Reverse
    for(int i = myList.length - 1; i >= 0 ; i--) {
        System.out.print(myList[i] + " ");
    }
    
    System.out.println("");
    //calculate sum of all array elements
		int sum = 0;
		
		for(int i=0; i < myList.length; i++)
			sum = sum + myList[i];
		
		//calculate average value
		double average = sum * 1.0 / myList.length;
		System.out.println("Sum of all numbers is " + sum);
        System.out.println("Average is " + average);
        
        // Print elements greater than average 
        for (int i = 0; i < myList.length; i++)  
            if (myList[i] > average)  
                System.out.print(myList[i] + " ");
                System.out.print("are greater than the average");
                 
    }
                
}