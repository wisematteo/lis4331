import java.util.Scanner;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;


public class SimpleInterestCalculator
{
   public static void main(String args[])
   {
      Scanner sc = new Scanner(System.in);
      int principal = 0;
      double rate = 0;
      int years = 0;
      double calculation;
      double rateDecimal;
      boolean isValidNum = false; 
      boolean isTrueNum = false; 
      boolean isYearsNum = false;
    System.out.println("Program performs the following functions:");
    System.out.println("1. Calculates amount of money saved for a period of years, at a specified interest rate (i.e., yearly, non-compounded)");
    System.out.println("2. Returned amount is formatted in U.S. currency, and rounded to two decimal places.");
    System.out.println("");
    System.out.println("***Note:*** Program checks for non-numeric values, as well as only integer values for years.");
    System.out.println("");
    //first loop
    while (isValidNum == false)
    {
        System.out.print("Current principal: $");
            if (sc.hasNextInt())
            {
                principal = sc.nextInt();
                isValidNum = true;
            }
        else
        {
            System.out.println("Not valid number!");
            System.out.println();
            System.out.print("Please try again. ");
        }
        sc.nextLine();
    }
    
        
    System.out.println("");

    //second loop
    while (isTrueNum == false)
    {
        System.out.print("Interest Rate-per year: ");
            if (sc.hasNextDouble())
            {
                rate = sc.nextDouble();
                isTrueNum = true;
            }
        else
        {
            System.out.println("Not valid number!");
            System.out.println();
            System.out.print("Please try again. ");
        }
        sc.nextLine();
    }
    System.out.println("");
    //third loop
    while (isYearsNum == false)
    {
        System.out.print("Total time-in years: ");
            if (sc.hasNextInt())
            {
                years = sc.nextInt();
                isYearsNum = true;
            }
        else
        {
            System.out.println("Not valid integer!");
            System.out.println();
            System.out.print("Please try again. ");
        }
        sc.nextLine();
    }

    //Calculations
    rateDecimal = (rate/100); 
    calculation = principal * (1 + (rateDecimal * years));
    DecimalFormat dFormat = new DecimalFormat("####,###,###.00");

    
    
    //Results
    System.out.println("");     
    System.out.println("You will have saved $" + dFormat.format(calculation) + " in " + years + " years, at an interest rate of " + rate + "%");
   
    
  }
}