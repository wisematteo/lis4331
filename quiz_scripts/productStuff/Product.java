public class Product
{

private String productCode;
private String productDescription;
private Double productPrice;

public Product(){

    System.out.println("\nInside product default constructor.");
    productCode = "abc123";
    productDescription = "my widget";
    productPrice = 49.99;
}

public Product (String productCode, String productDescription, double productPrice){

    System.out.println("\nInside product constructor with parameters.");
    this.productCode= productCode;
    this.productDescription = productDescription;
    this.productPrice = productPrice;
    
}

public String getProductCode(){
    return productCode;
}

public void setProductCode(String pc){
    productCode = pc;
}

public String getProductDescription(){
    return productDescription;
}
public void setProductDescription(String pd){
    
    productDescription = pd; 
}

public double getProductPrice(){
    return productPrice;
}

public void setProductPrice(double pp){
    productPrice = pp; 
}
public void print() {
    System.out.println("Code: " + productCode + ", Description: " + productDescription + ", Price: $" + productPrice);
}

}