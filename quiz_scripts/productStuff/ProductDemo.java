import java.util.Scanner;
import java.text.DecimalFormat;

class ProductDemo{

public static void main(String[]args){
String code = "";
String description = "";
Double price = 0.00;
Scanner sc = new Scanner(System.in);

System.out.println("\n////goooblle gooble gooble gooble one of us one of us//////");

Product p1 = new Product();

System.out.println("Code: xyz");
System.out.println("Description: test widget");
System.out.println("Price: 49.99");

System.out.println("\n////goooblle gooble gooble gooble one of us one of us//////");

System.out.print("\nCode: ");
code = sc.nextLine();

System.out.print("Description: ");
description = sc.nextLine();

System.out.print("Price: $");
price = sc.nextDouble();


Product p2 = new Product(code, description, price);

System.out.println("\nCode = " + p2.getProductCode());
System.out.print("desciption: " + p2.getProductDescription());
System.out.println("");
System.out.print("price: " + p2.getProductPrice());
System.out.print("\n");

System.out.println("\n///below using setter methhods to pass literal values, then print() method to display values // \n");
p2.setProductCode("xyz");
p2.setProductDescription("test widget");
p2.setProductPrice(89.99);
p2.print();
}

}