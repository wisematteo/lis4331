class Book extends Product
{


private String productAuthor; 

public Book(){

    super();
    System.out.println("\nInside product default constructor.");
    productAuthor = "charles";

}

public Book (String pc, String pd, double pp, String pa){

    super(pc, pd, pp);
    System.out.println("\nbelow employee constuctor with parameters");
    productAuthor = pa; 
    
}

public String getProductAuthor(){
    return productAuthor;
}

public void setProductAuthor(String pa){
    productAuthor = pa;
}

public void print() {
    System.out.println("Author: " + productAuthor);
}

}