import java.util.Scanner;
import java.text.DecimalFormat;

class BookDemo{
public static void main(String[]args){
String code = "";
String description = "";
Double price = 0.00;
String author = "";
Scanner sc = new Scanner(System.in);

System.out.println("\n////goooblle gooble gooble gooble one of us one of us//////");
Book p1 = new Book();
System.out.println("Code: hello world");
System.out.println("Description: test widget");
System.out.println("Price: 00.00");
System.out.println("\n////goooblle gooble gooble gooble one of us one of us//////");
System.out.print("\nCode: ");

code = sc.nextLine();

System.out.print("Description: ");
description = sc.nextLine();

System.out.print("Price: $");
price = sc.nextDouble();


Product p2 = new Product(code, description, price);
System.out.println("\nCode = " + p2.getProductCode());
System.out.print("desciption: " + p2.getProductDescription());
System.out.println("");
System.out.print("price: " + p2.getProductPrice());
System.out.println("\n///below using setter methhods to pass literal values, then print() method to display values // \n");

p2.setProductCode("xyz");
p2.setProductDescription("test widget");
p2.setProductPrice(89.99);
p2.print();
System.out.println();

System.out.print("\n////below are derived class default constructor///////");
Book e1 = new Book();
System.out.println("\nCode = xyz"); 
System.out.println("Description = test widget"); 
System.out.println("Price = charles");
System.out.println("Author = Orwell");
//System.out.println("\nOr");
//e1.print();
System.out.println("\n/////below are derived class user entered values////");

System.out.println("Code: ");
code = sc.nextLine();
System.out.println("");

System.out.println("Description: ");
description = sc.nextLine();


System.out.print("Price: $");
price = sc.nextDouble();

System.out.print("Author: ");
author = sc.nextLine();

Book e2 = new Book(code, description, price, author);
System.out.println("\nCode = " + e2.getProductCode()); 
System.out.println("Description = " + e2.getProductDescription()); 
System.out.print("Price = " + e2.getProductPrice());
System.out.println("Author = " + e2.getProductAuthor());
//System.out.println("\nOr");
e2.print();

System.out.print("\n/////above are derived class user entered values////");
}

}