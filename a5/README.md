# LIS4331 - Advanced Mobile Web Application Development

## Matteo Wise

### Assignment 5 Requirements: 

1. Include splash screen with app title andlist of articles.
2. Must find and use your ownRSS feed.
3. Must add background color(s) or theme.
4. Create and displaylauncher icon image.                              

##README.md file should include the following item(see screenshots before).
1. Coursetitle, your name, assignment requirements, as per A1;
2. Screenshotofrunning application’s splash screen(list of articles –activity_items.xml);
3. Screenshotofrunning application’s individual article(activity_item.xml);
4. Screenshotsofrunning application’sdefault browser(article link);                                                       



#### assignment screen shots:
| *Screenshot of running application*                          | *Screenshot of running application*                              |
|--------------------------------------------------------------|------------------------------------------------------------------|
|                                                              |                                                                  |  |                                                              |                                                                  |
| ![GUI - Menu](images/a.png)                                  | ![GUI - Menu](images/b.png)                                      |
| ![GUI - Menu](images/c.png)                                  | ![GUI - Menu](images/d.png)                                      |

